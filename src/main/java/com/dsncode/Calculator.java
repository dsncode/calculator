package com.dsncode;

import java.util.LinkedList;
import java.util.function.BiFunction;

public class Calculator {

    LinkedList<String> commands = new LinkedList<>();

    BiFunction<Double, Double, Double> add = (a, b) -> a + b;
    BiFunction<Double, Double, Double> substract = (a, b) -> a - b;
    BiFunction<Double, Double, Double> multiply = (a, b) -> a * b;
    BiFunction<Double, Double, Double> divide = (a, b) -> a / b;

    public double compute() {
        double total = Double.parseDouble(commands.remove(0));
        BiFunction<Double, Double, Double> nextCommand = null;

        for (String cmd : commands) {
            try {
                double aux = Double.parseDouble(cmd);
                total = nextCommand.apply(total, aux);
            } catch (Exception e) {
                // not a number
                switch (cmd) {
                case "+":
                    nextCommand = this.add;
                    break;
                case "-":
                    nextCommand = this.substract;
                    break;
                case "*":
                    nextCommand = this.multiply;
                    break;
                case "/":
                    nextCommand = this.divide;
                    break;
                }
            }
        }
        return total;
    }

    public void addToCommandBuffer(String cmd) {
        this.commands.add(cmd);
    }

}