Feature: a calculator, should add, substract, divide and multiply

    As a user,
    I would like to have a calculator that can add, divide and multiply
    So that, I can see the result on the screen

    Scenario: add two numbers
        Given a working calculator
        When the user press "1"
        And the user press "+"
        And the user press "5"
        And the user press "="
        Then the calculator should print 6.0

    Scenario: divide two numbers
        Given a working calculator
        When the user press "10"
        And the user press "/"
        And the user press "5"
        And the user press "="
        Then the calculator should print 2.0