package com.dsncode;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.junit.Assert.fail;

import com.dsncode.Calculator;

public class Stepdefs {

    Calculator calculator;
    double total;

    @Given("a working calculator")
    public void a_working_calculator() {
        this.calculator = new Calculator();
    }

    @When("the user press {string}")
    public void the_user_press(String string) {

        if (string.equals("=")) {
            this.total = calculator.compute();
        } else {
            calculator.addToCommandBuffer(string);
        }
    }

    @Then("the calculator should print {double}")
    public void the_calculator_should_print(double expectedTotal) {
        if (expectedTotal != this.total) {
            fail("wrong calculation. it should be " + expectedTotal + " instead of " + this.total);
        }
    }
}
